DROP DATABASE IF EXISTS `agenda`;
CREATE DATABASE `agenda`;
USE agenda;
CREATE TABLE `tiposcontacto`
(
  `idTipoContacto` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(20) NOT NULL,
  PRIMARY KEY (`idTipoContacto`)
);
INSERT INTO tiposcontacto(Tipo) VALUES("");
CREATE TABLE `provincias`
(
  `idProvincia` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Pais` varchar(45) NOT NULL,
  PRIMARY KEY (`idProvincia`)
);
INSERT INTO provincias(Nombre, Pais) VALUES("", "");
CREATE TABLE `domicilios`
(
  `idDomicilio` int(11) NOT NULL AUTO_INCREMENT,
  `Calle` varchar(45) NOT NULL,
  `Altura` int(5) NOT NULL,
  `Piso` varchar(3),
  `Departamento` varchar(10),
  `Localidad` varchar(45) NOT NULL,
  `idProvincia` int(11) NOT NULL,
  PRIMARY KEY (`idDomicilio`),
  FOREIGN KEY (idProvincia) REFERENCES provincias(idProvincia)
);
INSERT INTO domicilios(Calle, Altura, Piso, Departamento, Localidad, idProvincia) VALUES("", -1, "", "", "", 1);
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `idDomicilio` int(11),
  `Email` varchar(45),
  `Cumpleanos` varchar(12),
  `idTipoContacto` int(11),
  `deporteFavorito` varchar(45),
  PRIMARY KEY (`idPersona`),
  FOREIGN KEY (idDomicilio) REFERENCES domicilios(idDomicilio),
  FOREIGN KEY (idTipoContacto) REFERENCES tiposcontacto(idTipoContacto)
);