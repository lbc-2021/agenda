package dto;

public class DomicilioDTO {
	private int idDomicilio;
	private String calle;
	private int altura;
	private String piso;
	private String departamento;
	private String localidad;
	private int idProvincia;

	public DomicilioDTO(int idDomicilio, String calle, int altura, String piso, String departamento, String localidad,
			int idProvincia) {
		this.idDomicilio = idDomicilio;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.departamento = departamento;
		this.localidad = localidad;
		this.idProvincia = idProvincia;
	}

	public int getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(int idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public int getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(int idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getStringDomicilio(String provincia) {
		if (idDomicilio == 1) {
			return "";
		} else if (piso.length() == 0 && departamento.length() == 0) {
			return calle + " " + altura + ", " + localidad + ", " + provincia;
		} else if (piso.length() == 0 && departamento.length() > 0) {
			return calle + " " + altura + ", depto. " + departamento + ", " + localidad + ", " + provincia;
		} else {
			return calle + " " + altura + ", piso " + piso + ", depto. " + departamento + ", " + localidad + ", "
					+ provincia;
		}
	};
}
