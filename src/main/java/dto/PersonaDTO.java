package dto;

public class PersonaDTO {
	private int idPersona;
	private String nombre;
	private String telefono;
	private int idDomicilio;
	private String email;
	private String cumpleanos;
	private int idTipoContacto;
	private String deporteFavorito;
	private String localidad;

	public PersonaDTO(int idPersona, String nombre, String telefono, int idDomicilio, String email, String cumpleanos,
			int tipoContacto, String deporteFav) {
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.idDomicilio = idDomicilio;
		this.email = email;
		this.cumpleanos = cumpleanos;
		this.deporteFavorito = deporteFav;
		this.idTipoContacto = tipoContacto;
	}

	public int getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getIdDomicilio() {
		return this.idDomicilio;
	}

	public void setIdDomicilio(int id) {
		this.idDomicilio = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCumpleanos() {
		return cumpleanos;
	}

	public void setCumpleanos(String cumpleanos) {
		this.cumpleanos = cumpleanos;
	}

	public int getIdTipoContacto() {
		return idTipoContacto;
	}

	public void setIdTipoContacto(int tipo) {
		this.idTipoContacto = tipo;
	}

	public String getDeporteFavorito() {
		return deporteFavorito;
	}

	public void setDeporteFavorito(String deporteFavorito) {
		this.deporteFavorito = deporteFavorito;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
}
