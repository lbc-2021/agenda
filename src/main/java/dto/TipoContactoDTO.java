package dto;

public class TipoContactoDTO {

	private int idTipoContacto;
	private String tipo;

	public TipoContactoDTO(int id, String tipo) {
		this.idTipoContacto = id;
		this.tipo = tipo;
	}

	public String getTipo() {
		return this.tipo;

	}

	public int getIdTipoContacto() {
		return this.idTipoContacto;

	}

	public void setTipo(String tipo) {
		this.tipo = tipo;

	}

	public void setIdTipoContacto(int id) {
		this.idTipoContacto = id;

	}
}
