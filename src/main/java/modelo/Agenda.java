package modelo;

import java.sql.SQLException;
import java.util.List;

import dto.DomicilioDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;

public class Agenda {
	private PersonaDAO persona;
	private ProvinciaDAO provincia;
	private DomicilioDAO domicilio;
	private TipoContactoDAO tipoContacto;

	public Agenda(DAOAbstractFactory metodo_persistencia) {
		this.persona = metodo_persistencia.createPersonaDAO();
		this.provincia = metodo_persistencia.createProvinciaDAO();
		this.domicilio = metodo_persistencia.createDomicilioDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
	}

	public void agregarPersona(PersonaDTO nuevaPersona) {
		this.persona.insert(nuevaPersona);
	}

	public void agregarProvincia(ProvinciaDTO nuevaProvincia) {
		this.provincia.insert(nuevaProvincia);
	}

	public void agregarDomicilio(DomicilioDTO nuevoDomicilio) {
		this.domicilio.insert(nuevoDomicilio);
	}

	public void agregarTipoContacto(TipoContactoDTO tipo) {
		this.tipoContacto.insert(tipo);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) {
		this.persona.delete(persona_a_eliminar);
	}

	public void borrarProvincia(ProvinciaDTO provincia_a_eliminar) throws SQLException {
		this.provincia.delete(provincia_a_eliminar);
	}

	public void borrarDomicilio(DomicilioDTO domicilio_a_eliminar) throws SQLException {
		this.domicilio.delete(domicilio_a_eliminar);
	}

	public void borrarTipoContacto(TipoContactoDTO tipo_a_eliminar) throws SQLException {
		this.tipoContacto.delete(tipo_a_eliminar);
	}

	public List<PersonaDTO> obtenerPersonas() {
		return this.persona.readAll();
	}

	public List<PersonaDTO> obtenerPersonasMatch(String nombre) {
		return this.persona.read(nombre);
	}

	public List<ProvinciaDTO> obtenerProvinciasPorPais(String pais) {
		return this.provincia.readByCountry(pais);
	}

	public List<ProvinciaDTO> obtenerProvinciasPorId(int id) {
		return this.provincia.readById(id);
	}

	public List<ProvinciaDTO> obtenerProvinciasMatch(String nombre, String pais) {
		return this.provincia.read(nombre, pais);
	}

	public List<ProvinciaDTO> obtenerProvincias() {
		return this.provincia.readAll();
	}

	public List<DomicilioDTO> obtenerDomicilios() {
		return this.domicilio.readAll();
	}

	public List<DomicilioDTO> obtenerDomicilios(int idDomicilio) {
		return this.domicilio.read(idDomicilio);
	}

	public List<DomicilioDTO> obtenerDomicilios(String calle, int altura, String piso, String depto, String localidad,
			int idProvincia) {
		return this.domicilio.read(calle, altura, piso, depto, localidad, idProvincia);
	}

	public List<TipoContactoDTO> obtenerTiposContacto() {
		return this.tipoContacto.readAll();
	}

	public List<TipoContactoDTO> obtenerTiposContacto(int idTipoContacto) {
		return this.tipoContacto.read(idTipoContacto);
	}

	public List<TipoContactoDTO> obtenerTiposContacto(String tipoContacto) {
		return this.tipoContacto.read(tipoContacto);
	}

	public void editarPersona(PersonaDTO persona_a_editar) {
		this.persona.edit(persona_a_editar);
	}

	public void editarDomicilio(DomicilioDTO domicilio_a_editar) {
		this.domicilio.edit(domicilio_a_editar);
	}
}
