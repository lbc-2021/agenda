package presentacion.vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaProvincias extends JFrame {
	private static final String[] paises = { "Afganist�n", "Albania", "Alemania", "Andorra", "Angola",
			"Antigua y Barbuda", "Arabia Saudita", "Argelia", "Argentina", "Armenia", "Australia", "Austria",
			"Azerbaiy�n", "Bahamas", "Banglad�s", "Barbados", "Bar�in", "B�lgica", "Belice", "Ben�n", "Bielorrusia",
			"Birmania", "Bolivia", "Bosnia y Herzegovina", "Botsuana", "Brasil", "Brun�i", "Bulgaria", "Burkina Faso",
			"Burundi", "But�n", "Cabo Verde", "Camboya", "Camer�n", "Canad�", "Catar", "Chad", "Chile", "China",
			"Chipre", "Ciudad del Vaticano", "Colombia", "Comoras", "Corea del Norte", "Corea del Sur",
			"Costa de Marfil", "Costa Rica", "Croacia", "Cuba", "Dinamarca", "Dominica", "Ecuador", "Egipto",
			"El Salvador", "Emiratos �rabes Unidos", "Eritrea", "Eslovaquia", "Eslovenia", "Espa�a", "Estados Unidos",
			"Estonia", "Etiop�a", "Filipinas", "Finlandia", "Fiyi", "Francia", "Gab�n", "Gambia", "Georgia", "Ghana",
			"Granada", "Grecia", "Guatemala", "Guyana", "Guinea", "Guinea ecuatorial", "Guinea-Bis�u", "Hait�",
			"Honduras", "Hungr�a", "India", "Indonesia", "Irak", "Ir�n", "Irlanda", "Islandia", "Islas Marshall",
			"Islas Salom�n", "Israel", "Italia", "Jamaica", "Jap�n", "Jordania", "Kazajist�n", "Kenia", "Kirguist�n",
			"Kiribati", "Kuwait", "Laos", "Lesoto", "Letonia", "L�bano", "Liberia", "Libia", "Liechtenstein",
			"Lituania", "Luxemburgo", "Madagascar", "Malasia", "Malaui", "Maldivas", "Mal�", "Malta", "Marruecos",
			"Mauricio", "Mauritania", "M�xico", "Micronesia", "Moldavia", "M�naco", "Mongolia", "Montenegro",
			"Mozambique", "Namibia", "Nauru", "Nepal", "Nicaragua", "N�ger", "Nigeria", "Noruega", "Nueva Zelanda",
			"Om�n", "Pa�ses Bajos", "Pakist�n", "Palaos", "Panam�", "Pap�a Nueva Guinea", "Paraguay", "Per�", "Polonia",
			"Portugal", "Reino Unido", "Rep�blica Centroafricana", "Rep�blica Checa", "Rep�blica de Macedonia",
			"Rep�blica del Congo", "Rep�blica Democr�tica del Congo", "Rep�blica Dominicana", "Rep�blica Sudafricana",
			"Ruanda", "Ruman�a", "Rusia", "Samoa", "San Crist�bal y Nieves", "San Marino",
			"San Vicente y las Granadinas", "Santa Luc�a", "Santo Tom� y Pr�ncipe", "Senegal", "Serbia", "Seychelles",
			"Sierra Leona", "Singapur", "Siria", "Somalia", "Sri Lanka", "Suazilandia", "Sud�n", "Sud�n del Sur",
			"Suecia", "Suiza", "Surinam", "Tailandia", "Tanzania", "Tayikist�n", "Timor Oriental", "Togo", "Tonga",
			"Trinidad y Tobago", "T�nez", "Turkmenist�n", "Turqu�a", "Tuvalu", "Ucrania", "Uganda", "Uruguay",
			"Uzbekist�n", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Yibuti", "Zambia", "Zimbabue" };
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JComboBox<String> cbPaises;
	private JComboBox<String> cbProvincias;
	private JButton btnAgregarProvincia;
	private JButton btnBorrarProvincia;
	private static VentanaProvincias INSTANCE;
	private JTextField txtNombre;

	public static VentanaProvincias getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaProvincias();
			return new VentanaProvincias();
		} else
			return INSTANCE;
	}

	private VentanaProvincias() {
		super();
		setTitle("Gesti\u00F3n de provincias");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 394, 250);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 368, 189);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblPais = new JLabel("Pa�s");
		lblPais.setBounds(10, 26, 113, 14);
		panel.add(lblPais);

		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 67, 113, 14);
		panel.add(lblProvincia);

		cbPaises = new JComboBox<String>();
		cbPaises.setBounds(82, 22, 271, 22);
		panel.add(cbPaises);

		this.llenarPaises();

		cbProvincias = new JComboBox<String>();
		cbProvincias.setBounds(82, 63, 271, 22);
		panel.add(cbProvincias);

		btnAgregarProvincia = new JButton("Agregar");
		btnAgregarProvincia.setBounds(107, 155, 89, 23);
		panel.add(btnAgregarProvincia);

		btnBorrarProvincia = new JButton("Borrar seleccionada");
		btnBorrarProvincia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnBorrarProvincia.setBounds(183, 96, 175, 23);
		panel.add(btnBorrarProvincia);

		txtNombre = new JTextField();
		txtNombre.setBounds(10, 156, 86, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		JLabel lblAgregar = new JLabel("Agregar provincia al pa�s seleccionado");
		lblAgregar.setBounds(10, 130, 300, 14);
		panel.add(lblAgregar);

		this.setVisible(false);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JComboBox<String> getCbPaises() {
		return cbPaises;
	}

	public JComboBox<String> getCbProvincias() {
		return cbProvincias;
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JButton getBtnAgregarProvincia() {
		return btnAgregarProvincia;
	}

	public JButton getBtnBorrarProvincia() {
		return btnBorrarProvincia;
	}

	public void cerrar() {
		this.dispose();
	}

	private void llenarPaises() {
		for (String pais : paises) {
			cbPaises.addItem(pais);
		}
	}
}
