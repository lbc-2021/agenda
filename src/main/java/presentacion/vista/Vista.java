package presentacion.vista;

import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import dto.DomicilioDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;

public class Vista {
	private JFrame frmAgendaByJn;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnEditar;
	private JButton btnProvincias;
	private JButton btnTiposContacto;
	private JButton btnManual;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = { "Nombre y apellido", "Tel�fono", "Domicilio", "Email", "Cumplea�os",
			"Tipo de contacto", "Deporte favorito" };

	public Vista() {
		super();
		initialize();
	}

	private void initialize() {
		frmAgendaByJn = new JFrame();
		frmAgendaByJn.setTitle("Agenda by JCN Team");
		frmAgendaByJn.setBounds(100, 100, 1280, 720);
		frmAgendaByJn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgendaByJn.getContentPane().setLayout(null);
		frmAgendaByJn.setLocationRelativeTo(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1264, 681);
		frmAgendaByJn.getContentPane().add(panel);
		panel.setLayout(null);

		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 1244, 587);
		panel.add(spPersonas);

		JLabel lblCopyright = new JLabel("Developed by JCN Team \u00A9 2021");
		lblCopyright.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCopyright.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblCopyright.setBounds(1052, 656, 202, 14);
		panel.add(lblCopyright);

		modelPersonas = new DefaultTableModel(null, nombreColumnas) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaPersonas = new JTable(modelPersonas);

		spPersonas.setViewportView(tablaPersonas);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(227, 622, 89, 23);
		panel.add(btnAgregar);

		btnEditar = new JButton("Editar");
		btnEditar.setBounds(326, 622, 89, 23);
		panel.add(btnEditar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(425, 622, 89, 23);
		panel.add(btnBorrar);

		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(524, 622, 89, 23);
		panel.add(btnReporte);

		btnProvincias = new JButton("Provincias");
		btnProvincias.setBounds(623, 622, 118, 23);
		panel.add(btnProvincias);

		btnTiposContacto = new JButton("Tipos de contacto");
		btnTiposContacto.setBounds(751, 622, 138, 23);
		panel.add(btnTiposContacto);

		btnManual = new JButton("Manual de usuario");
		btnManual.setBounds(899, 622, 138, 23);
		panel.add(btnManual);
	}

	public void show() {
		this.frmAgendaByJn.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgendaByJn.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(null, "�Est�s seguro que quieres salir de la Agenda?",
						"Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					Conexion.getConexion().cerrarConexion();
					System.exit(0);
				}
			}
		});
		this.frmAgendaByJn.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnProvincias() {
		return btnProvincias;
	}

	public JButton getBtnTiposContacto() {
		return btnTiposContacto;
	}

	public JButton getBtnManual() {
		return btnManual;
	}

	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}

	public JTable getTablaPersonas() {
		return tablaPersonas;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void llenarTabla(List<PersonaDTO> personasEnTabla, List<DomicilioDTO> domiciliosEnTabla,
			List<ProvinciaDTO> provinciasEnTabla, List<TipoContactoDTO> tiposContactoEnTabla) {
		this.getModelPersonas().setRowCount(0); // Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());
		for (int i = 0; i < personasEnTabla.size(); i++) {
			String nombre = personasEnTabla.get(i).getNombre();
			String tel = personasEnTabla.get(i).getTelefono();
			String dom = domiciliosEnTabla.get(i).getStringDomicilio(
					provinciasEnTabla.get(i).getNombre() + ", " + provinciasEnTabla.get(i).getPais());
			String email = personasEnTabla.get(i).getEmail();
			String cumple = personasEnTabla.get(i).getCumpleanos();
			String tipo = tiposContactoEnTabla.get(i).getTipo();
			String deporte = personasEnTabla.get(i).getDeporteFavorito();
			Object[] fila = { nombre, tel, dom, email, cumple, tipo, deporte };
			this.getModelPersonas().addRow(fila);
		}
		this.resizeColumnas();
	}

	private void resizeColumnas() {
		tablaPersonas.getColumnModel().getColumn(0).setMinWidth(125);
		tablaPersonas.getColumnModel().getColumn(1).setMinWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setMaxWidth(100);
		tablaPersonas.getColumnModel().getColumn(2).setMinWidth(250);
		tablaPersonas.getColumnModel().getColumn(3).setMinWidth(175);
		tablaPersonas.getColumnModel().getColumn(4).setMinWidth(75);
		tablaPersonas.getColumnModel().getColumn(4).setMaxWidth(100);
		tablaPersonas.getColumnModel().getColumn(5).setMinWidth(100);
		tablaPersonas.getColumnModel().getColumn(5).setMaxWidth(150);
		tablaPersonas.getColumnModel().getColumn(6).setMinWidth(100);
		tablaPersonas.getColumnModel().getColumn(6).setMaxWidth(150);
	}
}
