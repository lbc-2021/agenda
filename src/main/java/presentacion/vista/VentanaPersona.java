package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

public class VentanaPersona extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField[] txtDomicilio;
	private JComboBox<String> cbProvincias;
	private JTextField txtEmail;
	private JDateChooser calendario;
	private JComboBox<String> cbTiposContacto;
	private JTextField txtDeporte;
	private JButton btnAgregarPersona;
	private JButton btnAgregarProvincia;
	private JButton btnAgregarTipo;
	private static VentanaPersona INSTANCE;

	public static VentanaPersona getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaPersona();
			return new VentanaPersona();
		} else
			return INSTANCE;
	}

	private VentanaPersona() {
		super();
		setTitle("Agregar contacto");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 343, 508);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 307, 447);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 21, 113, 14);
		panel.add(lblNombreYApellido);

		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 52, 113, 14);
		panel.add(lblTelfono);

		JLabel lblDomicilio = new JLabel("Domicilio");
		lblDomicilio.setBounds(10, 86, 113, 14);
		panel.add(lblDomicilio);

		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 116, 34, 14);
		panel.add(lblCalle);

		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(194, 116, 44, 14);
		panel.add(lblAltura);

		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 147, 44, 14);
		panel.add(lblPiso);

		JLabel lblDepartamento = new JLabel("Departamento");
		lblDepartamento.setBounds(133, 147, 91, 14);
		panel.add(lblDepartamento);

		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 175, 89, 14);
		panel.add(lblLocalidad);

		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 204, 89, 14);
		panel.add(lblProvincia);

		JLabel lblAgregar = new JLabel("�No est\u00E1 la provincia que buscas?");
		lblAgregar.setBounds(64, 237, 201, 14);
		panel.add(lblAgregar);

		JLabel lblEmail = new JLabel("Correo electr\u00F3nico");
		lblEmail.setBounds(10, 299, 124, 14);
		panel.add(lblEmail);

		JLabel lblFecha = new JLabel("Fecha de cumplea\u00F1os");
		lblFecha.setBounds(10, 330, 172, 14);
		panel.add(lblFecha);

		JLabel lblTipoContacto = new JLabel("Tipo de contacto");
		lblTipoContacto.setBounds(10, 362, 113, 14);
		panel.add(lblTipoContacto);

		JLabel lblDeporte = new JLabel("Deporte favorito");
		lblDeporte.setBounds(10, 390, 124, 14);
		panel.add(lblDeporte);

		txtNombre = new JTextField();
		txtNombre.setBounds(133, 18, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 49, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);

		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setBounds(208, 413, 89, 23);
		panel.add(btnAgregarPersona);

		txtDomicilio = new JTextField[5];
		txtDomicilio[0] = new JTextField();
		txtDomicilio[0].setColumns(10);
		txtDomicilio[0].setBounds(45, 113, 140, 20);
		panel.add(txtDomicilio[0]);
		txtDomicilio[1] = new JTextField();
		txtDomicilio[1].setColumns(10);
		txtDomicilio[1].setBounds(234, 113, 63, 20);
		panel.add(txtDomicilio[1]);
		txtDomicilio[2] = new JTextField();
		txtDomicilio[2].setColumns(10);
		txtDomicilio[2].setBounds(45, 144, 71, 20);
		panel.add(txtDomicilio[2]);
		txtDomicilio[3] = new JTextField();
		txtDomicilio[3].setColumns(10);
		txtDomicilio[3].setBounds(220, 144, 63, 20);
		panel.add(txtDomicilio[3]);
		txtDomicilio[4] = new JTextField();
		txtDomicilio[4].setColumns(10);
		txtDomicilio[4].setBounds(133, 172, 164, 20);
		panel.add(txtDomicilio[4]);

		cbProvincias = new JComboBox<String>();
		cbProvincias.setBounds(133, 200, 164, 22);
		panel.add(cbProvincias);

		btnAgregarProvincia = new JButton("Agregar provincia");
		btnAgregarProvincia.setBounds(85, 262, 140, 23);
		panel.add(btnAgregarProvincia);

		btnAgregarTipo = new JButton("+");
		btnAgregarTipo.setBounds(253, 358, 44, 23);
		panel.add(btnAgregarTipo);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(133, 296, 164, 20);
		panel.add(txtEmail);

		calendario = new JDateChooser();
		calendario.setBounds(192, 324, 105, 20);
		calendario.setDateFormatString("dd/MM/yyyy");
		panel.add(calendario);

		cbTiposContacto = new JComboBox<String>();
		cbTiposContacto.setBounds(119, 358, 124, 22);
		panel.add(cbTiposContacto);

		txtDeporte = new JTextField();
		txtDeporte.setColumns(10);
		txtDeporte.setBounds(133, 387, 164, 20);
		panel.add(txtDeporte);

		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() {
		return btnAgregarPersona;
	}

	public JTextField[] getTxtDomicilio() {
		return txtDomicilio;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public JDateChooser getCalendario() {
		return calendario;
	}

	public JComboBox<String> getCbProvincias() {
		return cbProvincias;
	}

	public JComboBox<String> getCbTiposContacto() {
		return cbTiposContacto;
	}

	public JTextField getTxtDeporte() {
		return txtDeporte;
	}

	public JButton getBtnAgregarProvincia() {
		return btnAgregarProvincia;
	}

	public JButton getBtnAgregarTipo() {
		return btnAgregarTipo;
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtDomicilio[0].setText(null);
		this.txtDomicilio[1].setText(null);
		this.txtDomicilio[2].setText(null);
		this.txtDomicilio[3].setText(null);
		this.txtDomicilio[4].setText(null);
		this.txtEmail.setText(null);
		this.calendario.setDate(null);
		this.txtDeporte.setText(null);
		this.dispose();
	}
}
