package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaLogin extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtUser;
	private JPasswordField txtPsw;
	private JButton btnIngresar;
	private JButton btnHelp;
	private static VentanaLogin INSTANCE;

	public static VentanaLogin getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaLogin();
			return new VentanaLogin();
		} else
			return INSTANCE;
	}

	private VentanaLogin() {
		super();
		setTitle("Autenticaci\u00F3n");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 150);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 284, 111);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblUser = new JLabel("Usuario");
		lblUser.setBounds(52, 14, 66, 14);
		panel.add(lblUser);

		JLabel lblPsw = new JLabel("Contrase\u00F1a");
		lblPsw.setBounds(52, 49, 66, 14);
		panel.add(lblPsw);

		txtUser = new JTextField();
		txtUser.setText("root");
		txtUser.setBounds(128, 11, 110, 20);
		panel.add(txtUser);
		txtUser.setColumns(10);

		txtPsw = new JPasswordField();
		txtPsw.setBounds(128, 46, 110, 20);
		panel.add(txtPsw);

		btnIngresar = new JButton("Ingresar");
		btnIngresar.setBounds(99, 77, 89, 23);
		panel.add(btnIngresar);
		
		btnHelp = new JButton("?");
		btnHelp.setBounds(229, 77, 45, 23);
		panel.add(btnHelp);

		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(null, "�Est�s seguro que quieres salir de la Agenda?",
						"Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					System.exit(0);
				}
			}
		});
		this.setVisible(true);
	}

	public void cerrar() {
		this.dispose();
	}

	public JTextField getTxtUser() {
		return txtUser;
	}

	public JTextField getTxtPsw() {
		return txtPsw;
	}

	public JButton getBtnIngresar() {
		return this.btnIngresar;
	}

	public JButton getBtnHelp() {
		return btnHelp;
	}
}
