package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class VentanaAviso extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblAviso;
	private JButton btnAceptar;
	private static VentanaAviso INSTANCE;

	public static VentanaAviso getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaAviso();
			return new VentanaAviso();
		} else
			return INSTANCE;
	}

	private VentanaAviso() {
		super();
		setTitle("Aviso");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 343, 200);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 307, 139);
		contentPane.add(panel);
		panel.setLayout(null);

		lblAviso = new JLabel("Aviso");
		lblAviso.setHorizontalAlignment(SwingConstants.CENTER);
		lblAviso.setBounds(0, 11, 297, 83);
		panel.add(lblAviso);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(107, 105, 89, 23);
		panel.add(btnAceptar);

		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public JLabel getLblAviso() {
		return lblAviso;
	}

	public void cerrar() {
		this.dispose();
	}
}
