package presentacion.vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaTiposContacto extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JComboBox<String> cbTipos;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private static VentanaTiposContacto INSTANCE;
	private JTextField txtNuevo;

	public static VentanaTiposContacto getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaTiposContacto();
			return new VentanaTiposContacto();
		} else
			return INSTANCE;
	}

	private VentanaTiposContacto() {
		super();
		setTitle("Gesti\u00F3n de tipos de contacto");

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 394, 209);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 368, 148);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblTipo = new JLabel("Tipo de contacto");
		lblTipo.setBounds(10, 26, 113, 14);
		panel.add(lblTipo);

		cbTipos = new JComboBox<String>();
		cbTipos.setBounds(123, 22, 230, 22);
		panel.add(cbTipos);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(106, 113, 89, 23);
		panel.add(btnAgregar);

		btnBorrar = new JButton("Borrar seleccionado");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnBorrar.setBounds(178, 55, 175, 23);
		panel.add(btnBorrar);

		txtNuevo = new JTextField();
		txtNuevo.setBounds(10, 114, 86, 20);
		panel.add(txtNuevo);
		txtNuevo.setColumns(10);

		JLabel lblAgregar = new JLabel("Agregar tipo de contacto");
		lblAgregar.setBounds(10, 89, 300, 14);
		panel.add(lblAgregar);

		this.setVisible(false);
	}

	public JTextField getTxtNombre() {
		return txtNuevo;
	}

	public JComboBox<String> getCbTipos() {
		return cbTipos;
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public void cerrar() {
		this.dispose();
	}
}
