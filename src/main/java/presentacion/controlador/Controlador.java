package presentacion.controlador;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import dto.DomicilioDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.TipoContactoDTO;
import modelo.Agenda;
import persistencia.conexion.Conexion;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaAviso;
import presentacion.vista.VentanaLogin;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaProvincias;
import presentacion.vista.VentanaTiposContacto;
import presentacion.vista.Vista;

public class Controlador implements ActionListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private ArrayList<DomicilioDTO> domiciliosEnTabla;
	private ArrayList<ProvinciaDTO> provinciasEnTabla;
	private ArrayList<TipoContactoDTO> tiposContactoEnTabla;
	private List<ProvinciaDTO> provinciasEnLista;
	private List<TipoContactoDTO> tiposContactoEnLista;
	private VentanaPersona ventanaPersona;
	private VentanaProvincias ventanaProvincias;
	private VentanaTiposContacto ventanaTiposContacto;
	private VentanaAviso ventanaAviso;
	private VentanaLogin ventanaLogin;
	private Agenda agenda;

	public Controlador(Vista vista, Agenda agenda) {
		this.ventanaLogin = VentanaLogin.getInstance();
		this.ventanaLogin.getBtnIngresar().addActionListener(a -> this.crearConexion(a));
		this.ventanaLogin.getBtnHelp().addActionListener(h -> this.openManual(h));
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnReporte().addActionListener(r -> mostrarReporte(r));
		this.vista.getBtnEditar().addActionListener(e -> ventanaEditarPersona(e));
		this.vista.getBtnProvincias().addActionListener(l -> ventanaProvincias(l));
		this.vista.getBtnTiposContacto().addActionListener(c -> ventanaTiposContacto(c));
		this.vista.getBtnManual().addActionListener(m -> openManual(m));
		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaPersona.getBtnAgregarProvincia().addActionListener(l -> ventanaProvincias(l));
		this.ventanaPersona.getBtnAgregarTipo().addActionListener(b -> ventanaTiposContacto(b));
		this.ventanaProvincias = VentanaProvincias.getInstance();
		this.ventanaProvincias.getCbPaises().addActionListener(c -> actualizarCbProvincias(c));
		this.ventanaProvincias.getBtnAgregarProvincia().addActionListener(d -> guardarProvincia(d));
		this.ventanaProvincias.getBtnBorrarProvincia().addActionListener(l -> borrarProvincia(l));
		this.ventanaTiposContacto = VentanaTiposContacto.getInstance();
		this.ventanaTiposContacto.getBtnAgregar().addActionListener(a -> guardarTipoContacto(a));
		this.ventanaTiposContacto.getBtnBorrar().addActionListener(d -> borrarTipoContacto(d));
		this.ventanaAviso = VentanaAviso.getInstance();
		this.ventanaAviso.getBtnAceptar().addActionListener(c -> this.ventanaAviso.cerrar());
		this.agenda = agenda;
	}

	private void ventanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.cerrar();
		this.actualizarCbProvinciasYPaises(a);
		this.actualizarCbTiposContacto(a, this.ventanaPersona.getCbTiposContacto(), true);
		this.ventanaPersona.getBtnAgregarPersona().setText("Agregar");
		this.ventanaPersona.setTitle("Agregar contacto");
		this.clearActionListeners(this.ventanaPersona.getBtnAgregarPersona());
		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> guardarPersona(p));
		this.ventanaPersona.mostrarVentana();
	}

	private void ventanaEditarPersona(ActionEvent e) {
		int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
		if (filaSeleccionada != -1) {
			JTextField nombre = this.ventanaPersona.getTxtNombre();
			JTextField telefono = this.ventanaPersona.getTxtTelefono();
			JTextField calle = this.ventanaPersona.getTxtDomicilio()[0];
			JTextField altura = this.ventanaPersona.getTxtDomicilio()[1];
			JTextField piso = this.ventanaPersona.getTxtDomicilio()[2];
			JTextField depto = this.ventanaPersona.getTxtDomicilio()[3];
			JTextField localidad = this.ventanaPersona.getTxtDomicilio()[4];
			JTextField email = this.ventanaPersona.getTxtEmail();
			JTextField deporte = this.ventanaPersona.getTxtDeporte();
			this.actualizarCbProvincias(e);
			this.actualizarCbProvinciasYPaises(e);
			this.actualizarCbTiposContacto(e, this.ventanaPersona.getCbTiposContacto(), true);
			nombre.setText(this.personasEnTabla.get(filaSeleccionada).getNombre());
			telefono.setText(this.personasEnTabla.get(filaSeleccionada).getTelefono());
			calle.setText(this.domiciliosEnTabla.get(filaSeleccionada).getCalle());
			if (this.domiciliosEnTabla.get(filaSeleccionada).getAltura() != -1)
				altura.setText(Integer.toString(this.domiciliosEnTabla.get(filaSeleccionada).getAltura()));
			piso.setText(this.domiciliosEnTabla.get(filaSeleccionada).getPiso());
			depto.setText(this.domiciliosEnTabla.get(filaSeleccionada).getDepartamento());
			localidad.setText(this.domiciliosEnTabla.get(filaSeleccionada).getLocalidad());
			this.ventanaPersona.getCbProvincias()
					.setSelectedItem(this.provinciasEnTabla.get(filaSeleccionada).getNombre() + ", "
							+ this.provinciasEnTabla.get(filaSeleccionada).getPais());
			email.setText(this.personasEnTabla.get(filaSeleccionada).getEmail());
			Date fechaCumple;
			try {
				fechaCumple = new SimpleDateFormat("dd/MM/yyyy")
						.parse(this.personasEnTabla.get(filaSeleccionada).getCumpleanos());
			} catch (ParseException e1) {
				fechaCumple = null;
			}
			this.ventanaPersona.getCalendario().setDate(fechaCumple);
			this.ventanaPersona.getCbTiposContacto()
					.setSelectedItem(this.tiposContactoEnTabla.get(filaSeleccionada).getTipo());
			deporte.setText(this.personasEnTabla.get(filaSeleccionada).getDeporteFavorito());
			this.ventanaPersona.getBtnAgregarPersona().setText("Guardar");
			this.ventanaPersona.setTitle("Editar contacto");
			this.clearActionListeners(this.ventanaPersona.getBtnAgregarPersona());
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(x -> editarPersona(x, filaSeleccionada));
			this.ventanaPersona.mostrarVentana();
		}
	}

	private void ventanaProvincias(ActionEvent l) {
		this.actualizarCbProvincias(l);
		this.ventanaProvincias.mostrarVentana();
	}

	private void ventanaTiposContacto(ActionEvent l) {
		this.actualizarCbTiposContacto(l, this.ventanaTiposContacto.getCbTipos(), false);
		this.ventanaTiposContacto.mostrarVentana();
	}

	private void ventanaAviso(ActionEvent a, String mensaje) {
		this.ventanaAviso.getLblAviso().setText(
				"<html><body style='text-align: center;  text-justify: inter-word;'>" + mensaje + "</body></html>");
		this.ventanaAviso.mostrarVentana();
	}

	private void guardarPersona(ActionEvent p) {
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String tel = ventanaPersona.getTxtTelefono().getText();
		int idDomicilio = 1;
		String email = ventanaPersona.getTxtEmail().getText();
		String fecha = "";
		int idTipoContacto = 1;
		String deporte = ventanaPersona.getTxtDeporte().getText();
		boolean correcto = this.validarNombreTelefono(p, nombre, tel);
		if (correcto) {
			int idProvincia = this.guardarDomicilio(p);
			if (idProvincia != -1) {
				if (idProvincia != -3) {
					if (idProvincia != -2) {
						String calle = ventanaPersona.getTxtDomicilio()[0].getText();
						String altura = ventanaPersona.getTxtDomicilio()[1].getText();
						String piso = ventanaPersona.getTxtDomicilio()[2].getText();
						String depto = ventanaPersona.getTxtDomicilio()[3].getText();
						String localidad = ventanaPersona.getTxtDomicilio()[4].getText();
						idDomicilio = this.agenda
								.obtenerDomicilios(calle, Integer.parseInt(altura), piso, depto, localidad, idProvincia)
								.get(0).getIdDomicilio();
						correcto = true;
					} else {
						correcto = false;
						this.ventanaAviso(p, "Debe seleccionar una provincia.");
					}
				} else {
					correcto = false;
				}
			}
			if (ventanaPersona.getCalendario().getDate() != null) {
				Integer dia = ventanaPersona.getCalendario().getCalendar().get(Calendar.DAY_OF_MONTH);
				Integer mes = ventanaPersona.getCalendario().getCalendar().get(Calendar.MONTH) + 1;
				Integer anio = ventanaPersona.getCalendario().getCalendar().get(Calendar.YEAR);
				if (dia != null && mes != null && anio != null)
					fecha += dia + "/" + mes + "/" + anio;
			}
			if (ventanaPersona.getCbTiposContacto().getSelectedItem().toString() != "Ninguno") {
				idTipoContacto = this.agenda
						.obtenerTiposContacto(ventanaPersona.getCbTiposContacto().getSelectedItem().toString()).get(0)
						.getIdTipoContacto();
			}
		}
		if (correcto) {
			int size = this.agenda.obtenerPersonasMatch(nombre).size();
			if (size == 0) {
				if (validarMail(p, email)) {
					PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, idDomicilio, email, fecha, idTipoContacto,
							deporte);
					this.agenda.agregarPersona(nuevaPersona);
					this.refrescarTabla();
					this.ventanaPersona.cerrar();
				}
			} else {
				this.ventanaAviso(p, "Ya existe una persona con el mismo nombre.");
			}
		}
	}

	private int guardarDomicilio(ActionEvent d) {
		String calle = ventanaPersona.getTxtDomicilio()[0].getText();
		String altura = ventanaPersona.getTxtDomicilio()[1].getText();
		String localidad = ventanaPersona.getTxtDomicilio()[4].getText();
		String piso = ventanaPersona.getTxtDomicilio()[2].getText();
		String depto = ventanaPersona.getTxtDomicilio()[3].getText();
		int provinciaSeleccionada = this.ventanaPersona.getCbProvincias().getSelectedIndex();
		if (!calle.equals("") || !localidad.equals("") || !altura.equals("") || provinciaSeleccionada != 0) {
			if (validarDomicilio(d, calle, altura, piso, depto, localidad)) {
				if (provinciaSeleccionada != 0) {
					this.provinciasEnLista = agenda.obtenerProvincias();
					int idProvincia = this.provinciasEnLista.get(provinciaSeleccionada).getIdProvincia();
					DomicilioDTO nuevoDomicilio = new DomicilioDTO(0, calle, Integer.parseInt(altura), piso, depto,
							localidad, idProvincia);
					if (this.agenda
							.obtenerDomicilios(calle, Integer.parseInt(altura), piso, depto, localidad, idProvincia)
							.size() == 0) {
						this.agenda.agregarDomicilio(nuevoDomicilio);
					}
					return this.provinciasEnLista.get(provinciaSeleccionada).getIdProvincia(); // retorno el id de la
																								// provincia del nuevo
																								// domicilio
				} else {
					return -2; // -2 significa que falto provincia
				}
			} else {
				return -3; // -3 significa que falto algun campo
			}
		}
		return -1; // -1 significa que no se va a agregar un nuevo domicilio
	}

	private void guardarProvincia(ActionEvent l) {
		String nombre = this.ventanaProvincias.getTxtNombre().getText();
		if (validarProvincia(l, nombre)) {
			String pais = this.ventanaProvincias.getCbPaises().getSelectedItem().toString();
			ProvinciaDTO nuevaProvincia = new ProvinciaDTO(0, nombre, pais);
			if (this.agenda.obtenerProvinciasMatch(nombre, pais).size() == 0) {
				this.agenda.agregarProvincia(nuevaProvincia);
				this.actualizarCbProvincias(l);
				this.actualizarCbProvinciasYPaises(l);
				this.ventanaProvincias.getTxtNombre().setText("");
			} else {
				this.ventanaAviso(l, "La provincia que est� intentando ingresar ya existe.");
			}
		}
	}

	private void guardarTipoContacto(ActionEvent d) {
		String tipo = ventanaTiposContacto.getTxtNombre().getText();
		if (validarTipoContacto(d, tipo)) {
			if (this.agenda.obtenerTiposContacto(tipo).size() == 0) {
				TipoContactoDTO nuevoTipo = new TipoContactoDTO(0, tipo);
				this.agenda.agregarTipoContacto(nuevoTipo);
				this.actualizarCbTiposContacto(d, this.ventanaTiposContacto.getCbTipos(), false);
				this.actualizarCbTiposContacto(d, this.ventanaPersona.getCbTiposContacto(), true);
				this.ventanaTiposContacto.getTxtNombre().setText("");
			} else
				this.ventanaAviso(d, "El tipo de contacto que est� intentando ingresar ya existe.");
		}
	}

	private void mostrarReporte(ActionEvent r) {
		List<PersonaDTO> personas = this.agenda.obtenerPersonas();
		List<PersonaDTO> ret = new ArrayList<PersonaDTO>();
		List<DomicilioDTO> domicilios = this.agenda.obtenerDomicilios();
		for (DomicilioDTO d : domicilios) {
			ProvinciaDTO provActual = this.agenda.obtenerProvinciasPorId(d.getIdProvincia()).get(0);
			String locActual = "Ninguna";
			if (!d.getLocalidad().equals(""))
				locActual = d.getLocalidad() + ", " + provActual.getNombre() + ", " + provActual.getPais();
			for (PersonaDTO p : personas) {
				if (locActual.equals(this.getLocalidadStringFromPerson(p)))
					p.setLocalidad(locActual);
				if (!ret.contains(p))
					ret.add(p);
			}
		}
		ReporteAgenda reporte = new ReporteAgenda(ret);
		reporte.mostrar();
	}

	private String getLocalidadStringFromPerson(PersonaDTO persona) {
		DomicilioDTO domicilio = this.agenda.obtenerDomicilios(persona.getIdDomicilio()).get(0);
		ProvinciaDTO provincia = this.agenda.obtenerProvinciasPorId(domicilio.getIdProvincia()).get(0);
		if (domicilio.getIdDomicilio() == 1)
			return "Ninguna";
		else
			return domicilio.getLocalidad() + ", " + provincia.getNombre() + ", " + provincia.getPais();
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			if (this.domiciliosEnTabla.get(fila).getIdDomicilio() != 1) {
				try {
					this.agenda.borrarDomicilio(this.domiciliosEnTabla.get(fila));
				} catch (SQLException e) {
					System.out.println(
							"El domicilio no puede ser eliminado debido a que est� siendo utilizado por un contacto.");
				}
			}
		}
		this.refrescarTabla();
	}

	public void borrarProvincia(ActionEvent s) {
		int provinciaSeleccionada = this.ventanaProvincias.getCbProvincias().getSelectedIndex();
		if (provinciaSeleccionada != -1) {
			this.provinciasEnLista = agenda
					.obtenerProvinciasPorPais(this.ventanaProvincias.getCbPaises().getSelectedItem().toString());
			if (this.provinciasEnLista.get(provinciaSeleccionada).getIdProvincia() != 1) {
				try {
					this.agenda.borrarProvincia(this.provinciasEnLista.get(provinciaSeleccionada));
				} catch (SQLException ex) {
					this.ventanaAviso(s,
							"Esta provincia no se puede eliminar debido a que se est� utilizando en un contacto.");
				}
			}
			this.actualizarCbProvincias(s);
			this.actualizarCbProvinciasYPaises(s);
		}
	}

	public void borrarTipoContacto(ActionEvent s) {
		int tipoSeleccionado = this.ventanaTiposContacto.getCbTipos().getSelectedIndex();
		if (tipoSeleccionado != -1) {
			this.tiposContactoEnLista = agenda.obtenerTiposContacto();
			if (this.tiposContactoEnLista.get(tipoSeleccionado + 1).getIdTipoContacto() != 1) {
				try {
					this.agenda.borrarTipoContacto(this.tiposContactoEnLista.get(tipoSeleccionado + 1));
				} catch (SQLException e) {
					this.ventanaAviso(s,
							"Este tipo de contacto no se puede eliminar debido a que se est� utilizando en un contacto.");
				}
			}
			this.actualizarCbTiposContacto(s, this.ventanaTiposContacto.getCbTipos(), false);
		}
	}

	public void editarPersona(ActionEvent e, int fila) {
		PersonaDTO persona = this.personasEnTabla.get(fila);
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String tel = ventanaPersona.getTxtTelefono().getText();
		int idDomicilio = 1;
		String email = this.ventanaPersona.getTxtEmail().getText();
		String fecha = "";
		int idTipoContacto = 1;
		String deporte = this.ventanaPersona.getTxtDeporte().getText();
		boolean correcto = validarNombreTelefono(e, nombre, tel);
		if (correcto) {
			int idProvincia = this.guardarDomicilio(e);
			if (idProvincia != -1) {
				if (idProvincia != -3) {
					if (idProvincia != -2) {
						String calle = ventanaPersona.getTxtDomicilio()[0].getText();
						String altura = ventanaPersona.getTxtDomicilio()[1].getText();
						String piso = ventanaPersona.getTxtDomicilio()[2].getText();
						String depto = ventanaPersona.getTxtDomicilio()[3].getText();
						String localidad = ventanaPersona.getTxtDomicilio()[4].getText();
						if (validarDomicilio(e, calle, altura, piso, depto, localidad)) {
							idDomicilio = this.agenda.obtenerDomicilios(calle, Integer.parseInt(altura), piso, depto,
									localidad, idProvincia).get(0).getIdDomicilio();
							correcto = true;
						} else
							correcto = false;
					} else {
						correcto = false;
						this.ventanaAviso(e, "Debe seleccionar una provincia.");
					}
				} else {
					correcto = false;
				}
			}
			if (ventanaPersona.getCalendario().getDate() != null) {
				Integer dia = ventanaPersona.getCalendario().getCalendar().get(Calendar.DAY_OF_MONTH);
				Integer mes = ventanaPersona.getCalendario().getCalendar().get(Calendar.MONTH) + 1;
				Integer anio = ventanaPersona.getCalendario().getCalendar().get(Calendar.YEAR);
				if (dia != null && mes != null && anio != null)
					fecha += dia + "/" + mes + "/" + anio;
			}
			this.tiposContactoEnLista = this.agenda.obtenerTiposContacto();
			if (ventanaPersona.getCbTiposContacto().getSelectedItem().toString() != "Ninguno")
				idTipoContacto = this.tiposContactoEnLista
						.get(this.ventanaPersona.getCbTiposContacto().getSelectedIndex()).getIdTipoContacto();
		}
		if (correcto && validarMail(e, email)) {
			persona.setNombre(nombre);
			persona.setTelefono(tel);
			persona.setEmail(email);
			int idDomicilioViejo = persona.getIdDomicilio();
			persona.setIdDomicilio(idDomicilio);
			persona.setCumpleanos(fecha);
			persona.setIdTipoContacto(idTipoContacto);
			persona.setDeporteFavorito(deporte);
			this.agenda.editarPersona(persona);
			if (idDomicilioViejo != 1 && idDomicilioViejo != persona.getIdDomicilio()) {
				try {
					this.agenda.borrarDomicilio(this.agenda.obtenerDomicilios(idDomicilioViejo).get(0));
				} catch (SQLException ex) {
					System.out.println(
							"El domicilio no puede ser eliminado debido a que est� siendo utilizado por un contacto.");
				}
			}
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		}
	}

	public void inicializar() {
		if (this.verificarJSONFile()) {
			this.leerJSON();
			this.refrescarTabla();
			this.vista.show();
		} else {
			this.ventanaLogin.mostrarVentana();
		}

	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		domiciliosEnTabla = new ArrayList<DomicilioDTO>();
		provinciasEnTabla = new ArrayList<ProvinciaDTO>();
		tiposContactoEnTabla = new ArrayList<TipoContactoDTO>();
		for (PersonaDTO p : personasEnTabla) {
			DomicilioDTO domicilioActual = this.agenda.obtenerDomicilios(p.getIdDomicilio()).get(0);
			TipoContactoDTO tipoContactoActual = this.agenda.obtenerTiposContacto(p.getIdTipoContacto()).get(0);
			domiciliosEnTabla.add(domicilioActual);
			provinciasEnTabla.add(this.agenda.obtenerProvinciasPorId(domicilioActual.getIdProvincia()).get(0));
			tiposContactoEnTabla.add(tipoContactoActual);
		}
		this.vista.llenarTabla(this.personasEnTabla, domiciliosEnTabla, provinciasEnTabla, tiposContactoEnTabla);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	private void clearActionListeners(JButton button) {
		for (ActionListener al : button.getActionListeners()) {
			button.removeActionListener(al);
		}
	}

	private void actualizarCbProvincias(ActionEvent c) {
		this.ventanaProvincias.getCbProvincias().removeAllItems();
		int paisSeleccionado = this.ventanaProvincias.getCbPaises().getSelectedIndex();
		if (paisSeleccionado != -1) {
			List<ProvinciaDTO> provincias = agenda
					.obtenerProvinciasPorPais(this.ventanaProvincias.getCbPaises().getSelectedItem().toString());
			for (ProvinciaDTO provincia : provincias) {
				this.ventanaProvincias.getCbProvincias().addItem(provincia.getNombre());
			}
		}
	}

	private void actualizarCbProvinciasYPaises(ActionEvent c) {
		this.ventanaPersona.getCbProvincias().removeAllItems();
		this.ventanaPersona.getCbProvincias().addItem("(Seleccionar)");
		List<ProvinciaDTO> provincias = agenda.obtenerProvincias();
		if (provincias.size() > 1) {
			for (ProvinciaDTO provincia : provincias) {
				if (provincia.getNombre().length() > 0)
					this.ventanaPersona.getCbProvincias().addItem(provincia.getNombre() + ", " + provincia.getPais());
			}
		}
	}

	private void actualizarCbTiposContacto(ActionEvent c, JComboBox<String> cb, boolean needsFirstItem) {
		cb.removeAllItems();
		if (needsFirstItem)
			cb.addItem("Ninguno");
		List<TipoContactoDTO> tipos = agenda.obtenerTiposContacto();
		if (tipos.size() > 1) {
			for (TipoContactoDTO tipo : tipos) {
				if (tipo.getTipo().length() > 0)
					cb.addItem(tipo.getTipo());
			}
		}
	}

	private boolean validarNombreTelefono(ActionEvent c, String nombre, String telefono) {
		Pattern patnombre = Pattern.compile("(([a-zA-Z�-�\\u00f1\\u00d1]|\\.)+\\s*)+");
		Matcher matnombre = patnombre.matcher(nombre);
		Pattern patTelefono = Pattern.compile("\\+*\\d{0,3}-*[0-9]+");
		Matcher matTelefono = patTelefono.matcher(telefono);
		if (!matnombre.matches()) {
			this.ventanaAviso(c, "El campo 'Nombre y apellido' no puede estar vac�o y debe contener letras.");
			return false;
		} else if (!matTelefono.matches()) {
			this.ventanaAviso(c, "El campo 'Tel�fono' no puede estar vac�o y debe contener n�meros.");
			return false;
		} else {
			return true;
		}
	}

	private boolean validarProvincia(ActionEvent c, String provincia) {
		Pattern patprovincia = Pattern.compile("(([a-zA-Z�-�\\u00f1\\u00d1]|\\.)+\\s*)+");
		Matcher matprovincia = patprovincia.matcher(provincia);
		if (!matprovincia.matches()) {
			this.ventanaAviso(c, "El campo 'Provincia' no puede estar vac�o y debe contener solamente letras.");
			return false;
		} else {
			return true;
		}
	}

	private boolean validarMail(ActionEvent c, String mail) {
		Pattern patmail = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@[a-z]+(\\.[a-z]*){1,2}|");
		Matcher matmail = patmail.matcher(mail);
		if (!matmail.matches()) {
			this.ventanaAviso(c, "El correo electr�nico es inv�lido.");
			return false;
		} else {
			return true;
		}
	}

	private boolean validarDomicilio(ActionEvent e, String calle, String altura, String piso, String depto,
			String localidad) {
		Pattern patLetra = Pattern.compile("(([a-z0-9A-Z�-�\\u00f1\\u00d1]|\\.)+\\s*)+");
		Pattern patNum = Pattern.compile("\\d+");
		Pattern patPiso = Pattern.compile("\\d*|");
		Matcher mat1 = patLetra.matcher(calle);
		Matcher mat2 = patNum.matcher(altura);
		Matcher mat3 = patPiso.matcher(piso);
		Matcher mat4 = patLetra.matcher(localidad);
		if (!mat1.matches()) {
			this.ventanaAviso(e, "El campo 'Calle' no puede estar vac�o.");
			return false;
		} else if (!mat2.matches()) {
			this.ventanaAviso(e, "El campo 'Altura' no puede estar vac�o y debe contener n�meros.");
			return false;
		} else if (!mat3.matches()) {
			this.ventanaAviso(e, "El campo 'Piso' debe contener n�meros.");
			return false;
		} else if (!mat4.matches()) {
			this.ventanaAviso(e, "El campo 'Localidad' no puede estar vac�o y debe contener solo letras.");
			return false;
		} else
			return true;
	}

	private boolean validarTipoContacto(ActionEvent e, String tipo) {
		Pattern pattipo = Pattern.compile("(([a-zA-Z�-�\\u00f1\\u00d1]|\\.)+\\s*)+");
		Matcher mattipo = pattipo.matcher(tipo);
		if (!mattipo.matches()) {
			this.ventanaAviso(e, "El campo 'Tipo' no puede estar vac�o y debe contener solamente letras.");
			return false;
		} else {
			return true;
		}
	}

	private boolean verificarJSONFile() {
		File archivo = new File("" + System.getenv("LOCALAPPDATA") + "\\Agenda\\login.json");
		if (!archivo.exists()) {
			return false;
		} else {
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	private void crearConexion(ActionEvent a) {
		String user = this.ventanaLogin.getTxtUser().getText();
		String pasw = this.ventanaLogin.getTxtPsw().getText();
		Conexion conexion = null;
		try {
			conexion = Conexion.getConexion(user, pasw, "");
		} catch (Exception e) {
			this.ventanaAviso(a, "Los datos ingresados son inv�lidos.");
		}
		if (conexion != null) {
			try {
				JSONObject obj = new JSONObject();
				obj.put("User", user);
				obj.put("Passw", pasw);
				File directorio = new File("" + System.getenv("LOCALAPPDATA") + "\\Agenda");
				directorio.mkdirs();
				FileWriter file = new FileWriter("" + System.getenv("LOCALAPPDATA") + "\\Agenda\\login.json");
				file.write(obj.toJSONString());
				file.flush();
				file.close();
				this.ventanaLogin.cerrar();
				conexion.runScriptCrearDB();
				conexion = Conexion.getConexion(user, pasw, "agenda");
				this.refrescarTabla();
				this.vista.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void leerJSON() {
		// Deserealizar
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("" + System.getenv("LOCALAPPDATA") + "\\Agenda\\login.json"));
			JSONObject jsonObj = (JSONObject) obj;
			String user = jsonObj.get("User").toString();
			String passw = jsonObj.get("Passw").toString();
			Conexion.getConexion(user, passw, "agenda");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void openManual(ActionEvent m) {
		if (Desktop.isDesktopSupported()) {
			try {
				File file = new File("manual\\manual.pdf");
				Desktop.getDesktop().open(file);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
