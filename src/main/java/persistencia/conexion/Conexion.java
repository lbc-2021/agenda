package persistencia.conexion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.Logger;

public class Conexion {
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);
	private String[] data = new String[2];

	private Conexion(String user, String psw, String db) throws Exception {
		this.data[0] = user;
		this.data[1] = psw;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + db, this.data[0],
					this.data[1]);
			this.connection.setAutoCommit(false);
			log.info("Conexi�n exitosa");
		} catch (Exception e) {
			log.error("Conexi�n fallida", e);
			throw e;
		}
	}

	public static Conexion getConexion(String user, String psw, String db) throws Exception {
		try {
			if (instancia == null) {
				instancia = new Conexion(user, psw, db);
			}
		} catch (Exception e) {
			throw e;
		}
		return instancia;
	}

	public static Conexion getConexion() {
		return instancia;
	}

	public Connection getSQLConexion() {
		return this.connection;
	}

	public void cerrarConexion() {
		try {
			this.connection.close();
			log.info("Conexi�n cerrada");
		} catch (SQLException e) {
			log.error("Error al cerrar la conexi�n!", e);
		}
		instancia = null;
	}

	public void runScriptCrearDB() {
		try {
			ScriptRunner runner = new ScriptRunner(getSQLConexion());
			String ruta = "scripts\\scriptAgenda.sql";
			Reader reader = new BufferedReader(new FileReader(ruta));
			runner.runScript(reader);
			log.info("La base de datos se ha creado correctamente!");
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		}
	}
}
