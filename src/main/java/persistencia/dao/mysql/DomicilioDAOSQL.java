package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.DomicilioDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DomicilioDAO;

public class DomicilioDAOSQL implements DomicilioDAO {
	private static final String insert = "INSERT INTO domicilios(idDomicilio, calle, altura, piso, departamento, localidad, idProvincia) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM domicilios WHERE idDomicilio = ?";
	private static final String readall = "SELECT * FROM domicilios";
	private static final String readByID = "SELECT * FROM domicilios WHERE idDomicilio = ?";
	private static final String readByMatch = "SELECT * FROM domicilios WHERE calle = ? AND altura = ? AND piso = ? AND departamento = ? AND localidad = ? AND idProvincia = ?";
	private static final String edit = "UPDATE domicilios SET calle = ?, altura = ?, piso = ?, departamento = ?, localidad = ?, idProvincia = ? WHERE idDomicilio = ?";

	public boolean insert(DomicilioDTO domicilio) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, domicilio.getIdDomicilio());
			statement.setString(2, domicilio.getCalle());
			statement.setInt(3, domicilio.getAltura());
			statement.setString(4, domicilio.getPiso());
			statement.setString(5, domicilio.getDepartamento());
			statement.setString(6, domicilio.getLocalidad());
			statement.setInt(7, domicilio.getIdProvincia());
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isInsertExitoso;
	}

	public boolean delete(DomicilioDTO domicilio_a_eliminar) throws SQLException {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, domicilio_a_eliminar.getIdDomicilio());
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isdeleteExitoso = true;
			}
		} catch (SQLException e) {
			throw e;
		}
		return isdeleteExitoso;
	}

	public List<DomicilioDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<DomicilioDTO> domicilios = new ArrayList<DomicilioDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				domicilios.add(getDomicilioDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return domicilios;
	}

	public List<DomicilioDTO> read(int idDomicilio) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<DomicilioDTO> domicilios = new ArrayList<DomicilioDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByID);
			statement.setInt(1, idDomicilio);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				domicilios.add(getDomicilioDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return domicilios;
	}

	public List<DomicilioDTO> read(String calle, int altura, String piso, String depto, String localidad,
			int idProvincia) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<DomicilioDTO> domicilios = new ArrayList<DomicilioDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByMatch);
			statement.setString(1, calle);
			statement.setInt(2, altura);
			statement.setString(3, piso);
			statement.setString(4, depto);
			statement.setString(5, localidad);
			statement.setInt(6, idProvincia);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				domicilios.add(getDomicilioDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return domicilios;
	}

	private DomicilioDTO getDomicilioDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("idDomicilio");
		String calle = resultSet.getString("Calle");
		int altura = resultSet.getInt("Altura");
		String piso = resultSet.getString("Piso");
		String depto = resultSet.getString("Departamento");
		String localidad = resultSet.getString("Localidad");
		int idProvincia = resultSet.getInt("idProvincia");
		return new DomicilioDTO(id, calle, altura, piso, depto, localidad, idProvincia);
	}

	public boolean edit(DomicilioDTO domicilio) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditExitoso = false;
		try {
			statement = conexion.prepareStatement(edit);
			statement.setString(1, domicilio.getCalle());
			statement.setInt(2, domicilio.getAltura());
			statement.setString(3, domicilio.getPiso());
			statement.setString(4, domicilio.getDepartamento());
			statement.setString(5, domicilio.getLocalidad());
			statement.setInt(6, domicilio.getIdProvincia());
			statement.setString(7, Integer.toString(domicilio.getIdDomicilio()));
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isEditExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isEditExitoso;
	}
}
