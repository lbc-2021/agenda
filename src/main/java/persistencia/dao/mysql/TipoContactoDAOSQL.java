package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO {

	private static final String insert = "INSERT INTO tiposcontacto (idTipoContacto, tipo) VALUES(?, ?) ON DUPLICATE KEY UPDATE tipo = ?";
	private static final String delete = "DELETE FROM tiposcontacto WHERE  idTipoContacto= ?";
	private static final String readall = "SELECT * FROM tiposcontacto";
	private static final String readByID = "SELECT * FROM tiposcontacto WHERE idTipoContacto = ?";
	private static final String readByMatch = "SELECT * FROM tiposcontacto WHERE tipo = ?";

	public boolean insert(TipoContactoDTO tipoContacto) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, tipoContacto.getIdTipoContacto());
			statement.setString(2, tipoContacto.getTipo());
			statement.setString(3, tipoContacto.getTipo());
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		System.out.println("Insert exitoso");
		return isInsertExitoso;
	}

	public boolean delete(TipoContactoDTO tipocontacto) throws SQLException{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, tipocontacto.getIdTipoContacto());
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isdeleteExitoso = true;
			}
		} catch (SQLException e) {
			throw e;
		}
		return isdeleteExitoso;
	}

	public List<TipoContactoDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<TipoContactoDTO> tiposcontacto = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tiposcontacto.add(getTipoContactoDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tiposcontacto;
	}

	public List<TipoContactoDTO> read(int idTipoContacto) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<TipoContactoDTO> tiposcontactos = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByID);
			statement.setInt(1, idTipoContacto);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tiposcontactos.add(getTipoContactoDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tiposcontactos;
	}
	
	public List<TipoContactoDTO> read(String tipo) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<TipoContactoDTO> tiposcontactos = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByMatch);
			statement.setString(1, tipo);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tiposcontactos.add(getTipoContactoDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tiposcontactos;
	}
	
	private TipoContactoDTO getTipoContactoDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("idTipoContacto");
		String tipo = resultSet.getString("tipo");
		TipoContactoDTO tipocontacto = new TipoContactoDTO(id, tipo);
		return tipocontacto;
	}
}
