package persistencia.dao.interfaz;

import java.sql.SQLException;
import java.util.List;

import dto.ProvinciaDTO;

public interface ProvinciaDAO {

	public boolean insert(ProvinciaDTO provincia);

	public boolean delete(ProvinciaDTO provincia_a_eliminar) throws SQLException;

	public List<ProvinciaDTO> readByCountry(String pais);

	public List<ProvinciaDTO> readById(int id);

	public List<ProvinciaDTO> readAll();

	public List<ProvinciaDTO> read(String nombre, String pais);
}