package persistencia.dao.interfaz;

import java.sql.SQLException;
import java.util.List;

import dto.DomicilioDTO;

public interface DomicilioDAO {

	public boolean insert(DomicilioDTO domicilio);

	public boolean delete(DomicilioDTO domicilio_a_eliminar) throws SQLException;

	public List<DomicilioDTO> readAll();

	public List<DomicilioDTO> read(int idDomicilio);

	public List<DomicilioDTO> read(String calle, int altura, String piso, String depto, String localidad,
			int idProvincia);

	public boolean edit(DomicilioDTO domicilio_a_editar);
}