package persistencia.dao.interfaz;

public interface DAOAbstractFactory {
	public PersonaDAO createPersonaDAO();

	public ProvinciaDAO createProvinciaDAO();

	public DomicilioDAO createDomicilioDAO();

	public TipoContactoDAO createTipoContactoDAO();
}
