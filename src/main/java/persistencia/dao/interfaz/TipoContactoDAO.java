package persistencia.dao.interfaz;

import java.sql.SQLException;
import java.util.List;

import dto.TipoContactoDTO;

public interface TipoContactoDAO {

	public boolean insert(TipoContactoDTO tipoContacto);

	public boolean delete(TipoContactoDTO tipoContacto_a_eliminar) throws SQLException;

	public List<TipoContactoDTO> readAll();

	public List<TipoContactoDTO> read(int idTipoContacto);

	public List<TipoContactoDTO> read(String tipoContacto);
}
